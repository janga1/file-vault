# File-Vault

A toy application for encrypting and decrypting data with [PyCryptodome](https://pycryptodome.readthedocs.io/en/latest/index.html)'s ChaCha20Poly1305 implementation.

## Usage

`py filevault {encrypt, decrypt} [FILE]`

## License

See `LICENSE`