from argparse import ArgumentParser
from pathlib import Path
from getpass import getpass
from Crypto.Hash import SHA256
from Crypto.Cipher import ChaCha20_Poly1305


EXTENSION  = ".vault"

class FileVault():

    def __init__(self):
        
        parser = ArgumentParser(description='A toy example of ChaCha20Poly1305.')
        parser.add_argument('mode', type=str, choices=['encrypt','decrypt'], help='Encrypt or decrypt.')
        parser.add_argument('location', type=str, help='Location of file or directory.')
        args = parser.parse_args()

        password = getpass("Password:")
        key = SHA256.new(password.encode()).digest()

        if args.mode == "encrypt":
            self.encrypt(Path(args.location), key)
        else:
            self.decrypt(Path(args.location), key)
    
    @staticmethod
    def encrypt(fd, key):

        if fd.is_file():

            with open(fd, "rb") as i:
                txt       = i.read()
                cipher    = ChaCha20_Poly1305.new(key = key)
                data, tag = cipher.encrypt_and_digest(txt)
                ext       = fd.suffixes + [EXTENSION]

                with open(fd.with_suffix(''.join(ext)), "wb") as o:                             
                    o.write(cipher.nonce)
                    o.write(tag)
                    o.write(data)
                    o.close()

                i.close()

            fd.unlink()

    @staticmethod
    def decrypt(fd, key):

        if fd.is_file() and fd.suffix == EXTENSION:

            with open(fd, "rb") as i:
                nonce  = i.read(12)
                tag    = i.read(16)
                data   = i.read()
                cipher = ChaCha20_Poly1305.new(key = key, nonce = nonce)
                try:
                    txt    = cipher.decrypt_and_verify(data, tag)
                except:
                    exit("Incorrect password")
                with open(fd.with_suffix(''), "wb") as o:
                    o.write(txt)
                    o.close()

                i.close()
            fd.unlink()
            
if __name__ == "__main__": FileVault()